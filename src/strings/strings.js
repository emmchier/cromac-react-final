export const getString = {
  itemAboutFibopa: `
        FIBOPA es el único evento de Body Painting en América, 
        que reúne artistas maquilladores corporales conviviendo 
        con otras disciplinas en vivo. Necesitaba un sitio web que 
        lo represente y visibilizara los eventos próximos y ediciones 
        anteriores. El sitio consiste en una landing que tiene como principal 
        protagonista la forografía. Se busca contar una historia a 
        través de una diagramación que desafía la estructura tradicional, 
        pero respeta la jerarquía y la legibilidad. Una de las premisas 
        era tener siempre el botón de contacto visible para facilitar la 
        interacción del usuario y que pueda solicitar más info sobre el 
        evento o unirse a la convocatoria. Se desarrolló en HTML, CSS y JavaScript, adaptando el diseño a 
        todos los dispositivos. Para las animaciones usamos la librería 
        Animate, y se tuvo un cuidado minucioso con las imágenes para 
        lograr una carga rápida y mantener la resolución en alta.
    `,
};

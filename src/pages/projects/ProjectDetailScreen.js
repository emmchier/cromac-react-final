import React, { useMemo } from 'react';

import { Redirect, useParams } from 'react-router';
import { Link } from 'react-router-dom';
import { useMediaQueries } from '../../hooks/useMediaQueries';
import { getProjectById } from '../../selectors/getProjectById';
import { TagList } from '../../components/lists/TagList';
import { ACButton } from '../../components/acuarel/button/ACButton';

export const ProjectDetailScreen = ({ history }) => {
  const { projectId } = useParams();

  const project = useMemo(() => getProjectById(projectId), [projectId]);

  const { isMobile } = useMediaQueries();

  const {
    itemName,
    itemImgHeaderDesk,
    itemImgHeaderRes,
    itemProjectDesc,
    itemTags,
    itemImgMultidevice,
    itemAboutProject,
    itemProjectUrl,
    itemProjectUrlLink,
    itemImgSectionExample,
    alt,
  } = project;

  if (!project) {
    return <Redirect to="/" />;
  }

  const handleReturn = () => {
    history.goBack();
  };

  return (
    <div id="detailScreen" className="animate__animated animate__fadeIn">
      <div className="projectHeader AC-alignMiddleX">
        <img
          src={
            !isMobile
              ? `./../assets/projects/detail/headers/desk/${itemImgHeaderDesk}.png`
              : `./../assets/projects/detail/headers/res/${itemImgHeaderRes}.png`
          }
          alt={alt}
        />
      </div>
      <section className="detail__detailInfoSection AC-autoHeight">
        <div className="container">
          <div className="row">
            <div className="col-sm-12 col-lg-6">
              <ACButton
                onClick={handleReturn}
                btnTitle={'Volver'}
                classes={'btnBack AC-button AC-button__text'}
                btnIcon={'arrow_backward'}
                isIconRightVisible={true}
              />
              <h3 className="projectName">{itemName}</h3>
              <p className="projectDesc">{itemProjectDesc}</p>
            </div>
            <div className="col-sm-12 col-lg-6">
              <TagList orientation={'horizontal'} itemTags={itemTags} />
            </div>
          </div>
        </div>
      </section>
      <section className="detail__multideviceSection AC-autoHeight">
        <div className="container">
          <img
            src={`./../assets/projects/detail/multidevice/${itemImgMultidevice}.png`}
            alt={alt}
          />
        </div>
      </section>
      <section className="detail__aboutProjectSection AC-autoHeight">
        <div className="container">
          <div className="row">
            <div className="col-sm-12 col-lg-6">
              <h3 className="about__title">¿De qué se trató el proyecto?</h3>
              <div className="about__footer">
                <p>¿Querés ver cómo quedó?</p>
                <Link to={itemProjectUrlLink} target="_blank">
                  {itemProjectUrl}
                </Link>
              </div>
            </div>
            <div className="col-sm-12 col-lg-6">
              <p>{itemAboutProject}</p>
            </div>
          </div>
        </div>
      </section>
      <section className="detail__projectExampleSection AC-autoHeight">
        <img
          src={`./../assets/projects/detail/section-examples/${itemImgSectionExample}.png`}
          alt={alt}
        />
      </section>
    </div>
  );
};

import React from 'react';

import { ACButton } from '../../components/acuarel/button/ACButton';
import { ACHeader } from '../../components/acuarel/header/ACHeader';
import { ProjectList } from '../../components/lists/ProjectList';

export const ProjectsScreen = ({ history }) => {
  const handleReturn = () => {
    history.goBack();
  };

  return (
    <div id="projectScreen" className="animate__animated animate__fadeIn">
      <div class="projectScreen__header">
        <ACButton
          onClick={handleReturn}
          btnTitle={'Volver'}
          classes={'btnBack AC-button AC-button__text'}
          btnIcon={'arrow_backward'}
          isIconRightVisible={true}
        />
        <ACHeader
          headerContentPosition={'center'}
          headerContent={
            <h3 className="projectTitle">
              Mirá en que proyectos
              <span className="AC-goDown">estuvimos trabajando.</span>
            </h3>
          }
          classes={'container projectHeader'}
        />
      </div>
      <section id="projectSection">
        <div className="container">
          <ProjectList isLimited={false} />
        </div>
      </section>
    </div>
  );
};

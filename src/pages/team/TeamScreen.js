import React from 'react';

import { ACButton } from '../../components/acuarel/button/ACButton';
import { ACHeader } from '../../components/acuarel/header/ACHeader';
import { TeamList } from '../../components/lists/TeamList';

export const TeamScreen = ({ history }) => {
  const handleReturn = () => {
    history.goBack();
  };

  return (
    <div id="teamScreen" className="animate__animated animate__fadeIn">
      <ACButton
        onClick={handleReturn}
        btnTitle={'Volver'}
        classes={'btnBack AC-button AC-button__text'}
        btnIcon={'arrow_backward'}
        isIconRightVisible={true}
      />
      <div className="team__header">
        <div className="container">
          <ACHeader
            headerContentPosition={'center'}
            headerContent={
              <h3 className="teamTitle">
                Detrás de todo producto digital
                <span className="AC-goDown">hay un gran equipo humano.</span>
              </h3>
            }
          />
        </div>
      </div>
      <section id="team__teamSection">
        <div className="container">
          <TeamList />
        </div>
      </section>
      <section id="team__missionSection">
        <div className="container">
          <h3>Nuestra misión.</h3>
          <div className="team__content">
            <p className="team__desc">
              Analizamos el proyecto, estimamos tiempos y te proponemos una solución integral acorde
              a tus necesidades. Si se trata de un proyecto grande, dividimos el proceso en etapas,
              acordando un MVP (producto mínimo viable). Analizamos el proyecto, estimamos tiempos y
              te proponemos una solución integral acorde a tus necesidades. Si se trata de un
              proyecto grande, dividimos el proceso en etapas, acordando un MVP (producto mínimo
              viable).
            </p>
            <img src="./../assets/team-profiles/team-icon.png" alt={'mission icon'} />
          </div>
        </div>
      </section>
    </div>
  );
};

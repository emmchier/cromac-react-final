import React from 'react';

export const ErrorScreen = () => {
  return (
    <div id="errorScreen" className="animate__animated animate__fadeIn">
      <div className="errorHeader AC-alignMiddleX">
        <div className="container">
          <h3 className="errorTitle">ERROR</h3>
        </div>
      </div>
    </div>
  );
};

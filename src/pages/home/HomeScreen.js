import React from 'react';

import { CoreComponent } from '../../components/ui/core/CoreComponent';
import { Line } from '../../components/ui/line/Line';
import { SparkComponent } from '../../components/ui/spark/SparkComponent';
import { SparkMultiple } from '../../components/ui/spark/SparkMultiple';
import { Sparks } from '../../components/ui/spark/Sparks';
import { ProjectList } from '../../components/lists/ProjectList';
import { ServiceList } from '../../components/lists/ServiceList';

import { ACButton } from '../../components/acuarel/button/ACButton';

export const HomeScreen = () => {
  const handleMeetUs = () => {};

  return (
    <div className="animate__animated animate__fadeIn">
      <section id="homeSection">
        <div className="container">
          <h1>
            Creamos soluciones <span>digitales que dejan marca</span>
          </h1>
          <ACButton
            onClick={handleMeetUs}
            btnTitle={'Conocenos'}
            classes={'btnMeetUs AC-button AC-button__text'}
            btnIcon={'arrow_downward'}
            isIconRightVisible={true}
          />
          <SparkComponent />
          <br></br>
          <SparkMultiple />
          <br></br>
          <Line />
          <br></br>
          <CoreComponent />
          <br></br>
          <Sparks number={5} />
        </div>
      </section>
      <section id="projectSection">
        <div className="container">
          <h2>En qué estuvimos trabajando</h2>
          <ProjectList isLimited={true} count={4} />
          <ACButton
            isLink={true}
            to={'/proyectos'}
            btnTitle={'Ver todos los proyectos'}
            classes={'btnToProjects AC-button AC-button__outlined'}
            btnIcon={'arrow_forward'}
            isIconRightVisible={true}
          />
        </div>
      </section>
      <section id="serviceSection">
        <div className="container">
          <h2>Qué hacemos</h2>
          <ServiceList />
          <h3>¿Tenes una idea?</h3>
          <ACButton
            isLink={true}
            to={'/contacto'}
            btnTitle={'Charlemos'}
            classes={'btnToContact AC-button AC-button__text'}
          />
        </div>
      </section>
    </div>
  );
};

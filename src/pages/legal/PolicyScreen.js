import React from 'react';

import { ACButton } from '../../components/acuarel/button/ACButton';

export const PolicyScreen = ({ history }) => {
  const handleReturn = () => {
    history.goBack();
  };

  return (
    <div id="policyScreen" className="animate__animated animate__fadeIn">
      <ACButton
        onClick={handleReturn}
        btnTitle={'Volver'}
        classes={'btnBack AC-button AC-button__text'}
        btnIcon={'arrow_backward'}
        isIconRightVisible={true}
      />
      <div className="policyHeader AC-alignMiddleX">
        <div className="container">
          <h3 className="policyTitle">POLÍTICAS DE PRIVACIDAD</h3>
        </div>
      </div>
    </div>
  );
};

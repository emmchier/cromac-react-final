import React from 'react';

import { ACButton } from '../../components/acuarel/button/ACButton';

export const TermsScreen = ({ history }) => {
  const handleReturn = () => {
    history.goBack();
  };

  return (
    <div id="termsScreen" className="animate__animated animate__fadeIn">
      <ACButton
        onClick={handleReturn}
        btnTitle={'Volver'}
        classes={'btnBack AC-button AC-button__text'}
        btnIcon={'arrow_backward'}
        isIconRightVisible={true}
      />
      <div className="termsHeader AC-alignMiddleX">
        <div className="container">
          <h3 className="termsTitle">Términos & condiciones</h3>
        </div>
      </div>
    </div>
  );
};

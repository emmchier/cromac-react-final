import React from 'react';
import { Link } from 'react-router-dom';

import { ACButton } from '../../components/acuarel/button/ACButton';
import { ACHeader } from '../../components/acuarel/header/ACHeader';
import { ServiceList } from '../../components/lists/ServiceList';
import { ProcedureList } from '../../components/procedure/ProcedureList';

export const ServicesScreen = ({ history }) => {
  const handleReturn = () => {
    history.goBack();
  };

  return (
    <div
      id="servicesScreen"
      className="
        animate__animated 
        animate__fadeIn"
    >
      <ACButton
        onClick={handleReturn}
        btnTitle={'Volver'}
        classes={'btnBack AC-button AC-button__text'}
        btnIcon={'arrow_backward'}
        isIconRightVisible={true}
      />
      <div className="services__header">
        <div className="container">
          <ACHeader
            headerContentPosition={'center'}
            headerContent={
              <h3 className="servicesTitle">
                ¿Querés brindarle una buena
                <span className="AC-goDown">experiencia a tus usuarios?</span>
                <span className="AC-goDown">Estás en el lugar indicado.</span>
              </h3>
            }
            classes={'servicesHeader'}
          />
        </div>
      </div>
      <section id="services__serviceSection">
        <div className="container">
          <h3>Qué hacemos</h3>
          <ServiceList />
        </div>
      </section>
      <section id="services__procedureSection">
        <div className="container">
          <h3>Cómo trabajamos.</h3>
          <ProcedureList />
        </div>
      </section>
      <div className="services__footer">
        <div className="container AC-alignMiddleX">
          <h3 className="servicesFooter">¿Tenes una idea?</h3>
          <Link to="/contacto">Charlemos</Link>
        </div>
      </div>
    </div>
  );
};

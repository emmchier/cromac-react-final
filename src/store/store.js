import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { projectReducer } from '../reducers/projectReducer';
import { serviceReducer } from '../reducers/serviceReducer';
import { uiReducer } from '../reducers/uiReducer';
import { teamReducer } from '../reducers/teamReducer';

const composeEnhancers =
  (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const reducers = combineReducers({
  ui: uiReducer,
  projects: projectReducer,
  services: serviceReducer,
  team: teamReducer,
});

export const store = createStore(reducers, composeEnhancers(applyMiddleware(thunk)));

import { getServices } from '../data/getData';

const initialState = {
  services: getServices,
};

export const serviceReducer = (state = initialState) => {
  return state;
};

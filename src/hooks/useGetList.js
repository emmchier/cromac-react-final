export const useGetList = () => {
  const networks = [
    {
      id: 0,
      icon: 'fab fa-behance',
      to: '//behance.net/',
      target: '_blank',
      isLink: true,
      isNetwork: true,
    },
    {
      id: 1,
      icon: 'fab fa-linkedin-in',
      to: '//linkedin.com/',
      target: '_blank',
      isLink: true,
      isNetwork: true,
    },
    {
      id: 2,
      icon: 'fab fa-facebook-f',
      to: '//facebook.com/',
      target: '_blank',
      isLink: true,
      isNetwork: true,
    },
    {
      id: 3,
      icon: 'fab fa-instagram',
      to: '//instagram.com/',
      target: '_blank',
      isLink: true,
      isNetwork: true,
    },
  ];

  const navList = [
    {
      id: 0,
      name: 'Inicio',
      to: '/',
      classes: 'navLink',
    },
    {
      id: 1,
      name: 'Proyectos',
      to: '/proyectos',
      classes: 'navLink',
    },
    {
      id: 2,
      name: 'Qué hacemos',
      to: '/que-hacemos',
      classes: 'navLink',
    },
    {
      id: 3,
      name: 'Equipo',
      to: '/equipo',
      classes: 'navLink',
    },
  ];

  const procedureList = [
    {
      itemId: 0,
      itemName: 'Conversación',
      itemDesc:
        'Primero, conocemos tu estado, necesidades y expectativas. Puede tratarse de uno o varios encuentros, los que sean necesarios según el caso.',
    },
    {
      itemId: 1,
      itemName: 'Análisis y propuesta',
      itemDesc:
        'Analizamos el proyecto, estimamos tiempos y te proponemos una solución integral acorde a tus necesidades. Si se trata de un proyecto grande, dividimos el proceso en etapas, acordando un MVP (producto mínimo viable).',
    },
    {
      itemId: 2,
      itemName: 'Diseño UX/UI',
      itemDesc:
        'Diseñamos productos digitales significativos que brinden la mejor experiencia para tus usuarios. Con prototipos y pruebas de usabilidad, iteramos el producto para encontrar la arquitectura y funcionalidades principales.',
    },
    {
      itemId: 3,
      itemName: 'Demo',
      itemDesc:
        'Realizamos una o varias demos para presentarle al cliente los resultados obtenidos, y acordamos como proseguimos hacía el desarrollo del producto.',
    },
    {
      itemId: 4,
      itemName: 'Desarrollo',
      itemDesc:
        'Desarrollamos el producto o servicio basado ​​en buenas prácticas en el desarrollo de software y aplicaciones. Trabajamos en el rendimiento, la accesibilidad y el posicionamiento del sitio en los motores de búsqueda.',
    },
    {
      itemId: 5,
      itemName: 'Testing y producción',
      itemDesc:
        'Preferimos relaciones a largo plazo y estamos presentes incluso después de entregar el proyecto. Lo probamos las veces que sea necesario, y podemos dar seguridad de que el resultado será el esperado.',
    },
  ];

  return {
    networks,
    navList,
    procedureList,
  };
};

import { useMediaQuery } from 'react-responsive';

export const useMediaQueries = () => {
  const isDeskXL = useMediaQuery({ minWidth: 1500 });
  const isDesk = useMediaQuery({ minWidth: 1200 });
  const isDeskSm = useMediaQuery({ minWidth: 993, maxWidth: 1199 });
  const isNote = useMediaQuery({ minWidth: 992 });
  const isTablet = useMediaQuery({ minWidth: 768 });
  const isMobile = useMediaQuery({ maxWidth: 600 });
  const isMobileSm = useMediaQuery({ minWidth: 576 });
  const isGalaxy5 = useMediaQuery({ minWidth: 360, maxWidth: 374 });
  const isIPhone5 = useMediaQuery({ minWidth: 280, maxWidth: 359 });

  return {
    isDeskXL,
    isDesk,
    isDeskSm,
    isNote,
    isTablet,
    isMobile,
    isMobileSm,
    isGalaxy5,
    isIPhone5,
  };
};

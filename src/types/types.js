export const types = {

    // UI
    uiSetError: '[UI] Set Error',
    uiRemoveError: '[UI] Remove Error',
    uiStartLoading: '[UI] Start loading',
    uiFinishLoading: '[UI] Finish loading',

    // Projects
    projectsGetAll: '[Projects] Get all projects',

    // Services
    servicesGetAll: '[Projects] Get all services'

}
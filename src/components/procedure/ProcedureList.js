import React from 'react';

import { useGetList } from '../../hooks/useGetList';
import { ACList } from '../acuarel/list/ACList';
import { ACListItem } from '../acuarel/list/ACListItem';
import { ProcedureItem } from './ProcedureItem';

export const ProcedureList = () => {
  const { procedureList } = useGetList();

  return (
    <ACList
      classes={'procedureList'}
      listItems={procedureList.map((procedure) => (
        <ACListItem
          key={procedure.itemId}
          classes={'procedureItem'}
          col={''}
          objectRender={
            <ProcedureItem procedureTitle={procedure.itemName} procedureDesc={procedure.itemDesc} />
          }
        />
      ))}
    />
  );
};

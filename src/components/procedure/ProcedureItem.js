import React from 'react';

import { SparkComponent } from '../ui/spark/SparkComponent';

export const ProcedureItem = ({ procedureTitle, procedureDesc }) => {
  return (
    <li className="procedureItem AC-alignX">
      <SparkComponent />
      <div className="row">
        <div className="col-sm-12 col-lg-5">
          <h3>{procedureTitle}</h3>
        </div>
        <div className="col-sm-12 col-lg-7">
          <p>{procedureDesc}</p>
        </div>
      </div>
    </li>
  );
};

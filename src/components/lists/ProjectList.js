import React from 'react';

import { useDispatch } from 'react-redux';
import { getAllProjects } from '../../actions/project';
import { Link } from 'react-router-dom';
import { ACList } from '../acuarel/list/ACList';
import { ACListItem } from '../acuarel/list/ACListItem';
import { ACCard } from '../acuarel/card/ACCard';

export const ProjectList = ({ isLimited = false, count = 0 }) => {
  const dispatch = useDispatch();

  const { payload } = dispatch(getAllProjects());

  const { projectList } = payload;

  const limitedList = projectList.slice(0, count);

  return (
    <ACList
      orientation={'horizontal'}
      classes={'projectList row'}
      listItems={(isLimited ? limitedList : projectList).map((project, key) => (
        <ACListItem
          key={key}
          classes={'projectItem'}
          col={'col-sm-12 col-lg-6'}
          objectRender={
            <ACCard
              cardImage={`./assets/projects/gallery/${project.itemImg}.png`}
              cardTitle={project.itemName}
              cardDesc={project.itemDesc}
            />
          }
          isHover={true}
          objectHover={<Link to={`/proyecto/${project.itemId}`}>Detalle</Link>}
        />
      ))}
    />
  );
};

import React from 'react';

import { useGetList } from '../../hooks/useGetList';
import { ACList } from '../acuarel/list/ACList';
import { ACListItem } from '../acuarel/list/ACListItem';
import { ACIcon } from '../acuarel/icon/ACIcon';

export const NetworkList = ({ orientation = 'horizontal' }) => {
  const { networks } = useGetList();

  return (
    <ACList
      orientation={orientation}
      classes={'networkList'}
      listItems={networks.map((network) => (
        <ACListItem
          key={network.id}
          classes={'networkItem'}
          col={''}
          objectRender={
            <ACIcon
              to={network.to}
              icon={network.icon}
              target={network.target}
              isLink={network.isLink}
              isNetwork={network.isNetwork}
            />
          }
        />
      ))}
    />
  );
};

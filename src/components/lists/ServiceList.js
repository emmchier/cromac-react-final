import React from 'react';
import { useSelector } from 'react-redux';

import { ACList } from '../acuarel/list/ACList';
import { ACListItem } from '../acuarel/list/ACListItem';
import { ACCard } from '../acuarel/card/ACCard';

export const ServiceList = () => {
  const { services } = useSelector((state) => state.services);

  return (
    <ACList
      orientation={'horizontal'}
      classes={'serviceList row'}
      listItems={services.map((service) => (
        <ACListItem
          key={service.itemId}
          col={'col-sm-12 col-md-6 col-lg-4'}
          classes={'serviceItem'}
          objectRender={
            <ACCard
              cardImage={`./assets/projects/gallery/${service.itemImg}.png`}
              cardTitle={service.itemName}
              cardDesc={service.itemDesc}
            />
          }
        />
      ))}
    />
  );
};

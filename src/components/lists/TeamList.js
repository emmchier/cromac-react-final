import React from 'react';
import { useSelector } from 'react-redux';

import { ACList } from '../acuarel/list/ACList';
import { ACListItem } from '../acuarel/list/ACListItem';
import { ACCard } from '../acuarel/card/ACCard';

export const TeamList = () => {
  const { team } = useSelector((state) => state.team);

  return (
    <ACList
      orientation={'horizontal'}
      classes={'teamList row'}
      listItems={team.map((teamProfile) => (
        <ACListItem
          key={teamProfile.itemId}
          col={'col-sm-12 col-md-6 col-lg-4'}
          classes={'teamItem'}
          objectRender={
            <ACCard
              cardImage={`./assets/team-profiles/${teamProfile.itemImg}.png`}
              cardTitle={teamProfile.itemName}
              cardDesc={teamProfile.itemDesc}
            />
          }
        />
      ))}
    />
  );
};

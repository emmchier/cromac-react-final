import React from 'react';

import { ACList } from '../acuarel/list/ACList';
import { ACListItem } from '../acuarel/list/ACListItem';
import { ACTag } from '../acuarel/tag/ACTag';

export const TagList = ({ orientation = 'horiztontal', itemTags = [] }) => {
  return (
    <ACList
      orientation={orientation}
      classes={'tagList'}
      listItems={itemTags.map((tag) => (
        <ACListItem
          key={tag.tagId}
          classes={'tagItem'}
          objectRender={<ACTag tagName={tag.tagName} />}
        />
      ))}
    />
  );
};

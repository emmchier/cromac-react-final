import React from 'react';

import { useGetList } from '../../hooks/useGetList';
import { ACList } from '../acuarel/list/ACList';
import { ACListItem } from '../acuarel/list/ACListItem';
import { NavLink } from 'react-router-dom';

export const NavList = ({ orientation = 'vertical', onClick }) => {
  const { navList } = useGetList();

  return (
    <ACList
      classes={'sidenavList'}
      orientation={orientation}
      listItems={navList.map((nav) => (
        <ACListItem
          key={nav.id}
          classes={'sidenavItem'}
          col={''}
          objectRender={
            <NavLink to={nav.to} onClick={onClick}>
              {nav.name}
            </NavLink>
          }
        />
      ))}
    />
  );
};

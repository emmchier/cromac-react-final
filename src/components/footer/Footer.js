import React from 'react';

import { Link } from 'react-router-dom';
import { ACButton } from '../acuarel/button/ACButton';

import { NetworkList } from '../lists/NetworkList';
import { NavList } from '../lists/NavList';

export const Footer = () => {
  return (
    <footer className="footerContent">
      <div className="container">
        <div className="row">
          <div className="col-sm-12 col-md-6 col-lg-4 colFooter colFooter--start">
            <div className="colFooter__content">
              <img src={`./assets/brand/brand.svg`} alt={'brand logo de Cromac Devs'} />
              <ACButton
                isLink={true}
                to={'/contacto'}
                btnTitle={'Charlemos'}
                classes={'btnContact AC-button AC-button__outlined'}
                btnIcon={'arrow_forward'}
                isIconRightVisible={true}
              />
            </div>
          </div>
          <div className="col-sm-12 col-md-6 col-lg-4 colFooter colFooter--center">
            <div className="colFooter__content">
              <h4>Estamos en</h4>
              <p>
                La Plata, Buenos Aires. <span>Argentina.</span>
              </p>
              <h4>Seguinos</h4>
              <NetworkList />
            </div>
          </div>
          <div className="col-sm-12 col-md-6 col-lg-4 colFooter colFooter--end">
            <div className="colFooter__content">
              <NavList />
            </div>
          </div>
        </div>
        <div className="copyright">
          <div className="row">
            <div className="col-sm-12 col-lg-6 colFooter colFooter--start">
              <div className="colFooter__content">
                <Link to="/politicas-de-privacidad">Políticas de privacidad</Link>
                <span className="separator"> | </span>
                <Link to="/terminos-y-condiciones">Términos & condiciones</Link>
              </div>
            </div>
            <div className="col-sm-12 col-lg-6 colFooter colFooter--end">
              <p className="colFooter__content">
                {new Date().getFullYear()} &#169 Cromac Devs
                <span className="separator"> | </span>
                Todos los derechos reservados
              </p>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

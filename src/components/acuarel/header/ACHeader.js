import React from 'react';

export const ACHeader = ({
  headerContentPosition = 'center',
  headerContent = [],
  classes = '',
}) => {
  return (
    <div className={`AC-header ${classes}`}>
      <div
        className={`AC-header__content AC-header__content--${headerContentPosition} AC-centerMiddle`}
      >
        {headerContent}
      </div>
    </div>
  );
};

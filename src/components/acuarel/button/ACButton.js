import React from 'react';
import { Link } from 'react-router-dom';

export const ACButton = ({
  onClick,
  btnTitle = 'Button',
  classes = '',
  to = '#!',
  btnIcon = 'pets',
  isIconLeftVisible = false,
  isIconRightVisible = false,
  btnType = '',
  btnTarget = '',
  isLink = false,
  isNetwork = false,
}) => {
  return !isLink ? (
    <button className={`${classes} AC-alignX`} onClick={onClick} type={btnType} target={btnTarget}>
      {!isNetwork
        ? isIconLeftVisible && (
            <span>
              <i className="material-icons AC-btnIcon mr-10"> {btnIcon} </i>
            </span>
          )
        : isIconLeftVisible && (
            <span>
              <i className={`${btnIcon} AC-btnIcon mr-10`}></i>
            </span>
          )}
      {btnTitle}
      {!isNetwork
        ? isIconRightVisible && <i className="material-icons AC-btnIcon ml-10"> {btnIcon} </i>
        : isIconRightVisible && <i className={`${btnIcon} AC-btnIcon ml-10`}></i>}
    </button>
  ) : (
    <Link
      className={`${classes} AC-alignX`}
      onClick={onClick}
      to={to}
      type={'button'}
      target={btnTarget}
    >
      {!isNetwork
        ? isIconLeftVisible && <i className="material-icons AC-btnIcon mr-10"> {btnIcon} </i>
        : isIconLeftVisible && <i className={`${btnIcon} AC-btnIcon mr-10`}></i>}
      {btnTitle}
      {!isNetwork
        ? isIconRightVisible && <i className="material-icons AC-btnIcon ml-10"> {btnIcon} </i>
        : isIconRightVisible && <i className={`${btnIcon} AC-btnIcon ml-10`}></i>}
    </Link>
  );
};

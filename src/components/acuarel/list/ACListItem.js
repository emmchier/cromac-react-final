import React, { useState } from 'react';

export const ACListItem = ({
  objectRender = {},
  objectHover = {},
  classes = '',
  col = '',
  isHover = false,
}) => {
  const [hover, setHover] = useState(false);

  return (
    <li
      className={`${classes} ${col}`}
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      {isHover && hover && objectHover}
      <div className="AC-itemContent">{objectRender}</div>
    </li>
  );
};

import React from 'react';

export const ACList = ({ listItems = {}, orientation = 'vertical', classes = '' }) => {
  return (
    <ul className={orientation === 'horizontal' ? `AC-alignX ${classes}` : `AC-alignY ${classes}`}>
      {listItems}
    </ul>
  );
};

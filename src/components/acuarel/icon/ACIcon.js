import React from 'react';
import { Link } from 'react-router-dom';

export const ACIcon = ({
  onClick,
  classes = '',
  to = '#!',
  icon = 'pets',
  target = '',
  isLink = false,
  isNetwork = false,
}) => {
  return !isLink ? (
    <div className={`AC-icon ${classes}`} onClick={onClick} target={target}>
      {!isNetwork ? (
        <i className="material-icons AC-icon"> {icon} </i>
      ) : (
        <i className={`${icon}`}></i>
      )}
    </div>
  ) : (
    <Link to={to} className={`AC-icon ${classes}`} onClick={onClick} target={target}>
      {!isNetwork ? (
        <i className="material-icons AC-icon"> {icon} </i>
      ) : (
        <i className={`${icon}`}></i>
      )}
    </Link>
  );
};

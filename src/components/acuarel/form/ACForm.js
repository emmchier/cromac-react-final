import React from 'react';

import validator from 'validator';
import { useForm } from '../../../hooks/useForm';
import { ACButton } from '../button/ACButton';
import { ACFormField } from '../formfield/ACFormField';

export const ACForm = () => {
  const [formValues, handleInputChange] = useForm({
    userName: '',
    email: '',
    subject: '',
    message: '',
  });

  const { userName, email, subject, message } = formValues;

  const handleLogin = (e) => {
    e.preventDefault();

    if (isFormValid()) {
      console.log('form OK!');
    }
  };

  const isFormValid = () => {
    if (userName.trim().length === 0) {
      //dispatch(setError('nombre requerido'));
      console.log('nombre requerido');
      return false;
    } else if (!validator.isEmail(email)) {
      console.log('email valido');
      //dispatch(setError('email no es valido'));
      return false;
    } else if (subject.trim().length === 0) {
      console.log('asunto requerido');
      //dispatch(setError('password incorrecto'));
      return false;
    } else if (message.trim().length === 0) {
      console.log('mensaje requerido');
      //dispatch(setError('password incorrecto'));
      return false;
    }
    //dispatch(removeError());
    return true;
  };

  return (
    <>
      <form className="AC-form" onSubmit={handleLogin}>
        {/* {
                    messageError &&
                    <div className="auth__alertError">
                        {messageError}
                    </div>
                } */}
        <ACFormField
          formInputValue={userName}
          showFormLabel={true}
          formForLabel={'userName'}
          formTitleLabel={'Nombre'}
          formInputType={'text'}
          formInputClasses={'inputName'}
          formInputId={'userNameme'}
          formInputName={'userName'}
          onChange={handleInputChange}
        />
        <ACFormField
          formInputValue={email}
          showFormLabel={true}
          formForLabel={'email'}
          formTitleLabel={'Email'}
          formInputType={'email'}
          formInputClasses={'inputEmail'}
          formInputId={'email'}
          formInputName={'email'}
          onChange={handleInputChange}
        />
        <ACFormField
          formInputValue={subject}
          showFormLabel={true}
          formForLabel={'subject'}
          formTitleLabel={'Asunto'}
          formInputType={'text'}
          formInputClasses={'inputSubject'}
          formInputId={'subject'}
          formInputName={'subject'}
          onChange={handleInputChange}
        />
        <ACFormField
          formInputValue={message}
          showFormLabel={true}
          formForLabel={'message'}
          formTitleLabel={'Mensaje'}
          formInputType={'text'}
          formInputClasses={'inputMessage'}
          formInputId={'message'}
          formInputName={'message'}
          onChange={handleInputChange}
        />
        <ACButton
          btnTitle={'Enviar'}
          btnType={'submit'}
          classes={'btnSubmit AC-button__contained'}
        />
      </form>
    </>
  );
};

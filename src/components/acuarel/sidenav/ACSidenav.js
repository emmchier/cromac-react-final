import React from 'react';

export const ACSidenav = ({
  isOpenSidenav = false,
  sidenavPosition = 'end',
  sidenavContent = [],
  onClickClose,
  closeToggle = [],
}) => {
  const handleSidenavPosition = () => {
    if (sidenavPosition === 'start') {
      return 'AC-sidenav--left';
    } else {
      return 'AC-sidenav--right';
    }
  };

  return (
    <div
      className={
        isOpenSidenav
          ? `AC-sidenav AC-sidenav--active ${handleSidenavPosition}`
          : `AC-sidenav ${handleSidenavPosition}`
      }
    >
      <div className="sidenavContent">
        <div className="AC-sidenav__closeButton" onClick={onClickClose}>
          {closeToggle}
        </div>
        <div className="AC-sidenav__content AC-centerMiddle">{sidenavContent}</div>
      </div>
    </div>
  );
};

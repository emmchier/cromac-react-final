import React from 'react';

export const ACOverlay = ({ isOpenOverlay = false, onClickOverlay }) => {
  return (
    <>
      {isOpenOverlay && (
        <div
          className={isOpenOverlay ? 'AC-overlay AC-overlay--active' : 'AC-overlay'}
          onClick={onClickOverlay}
        ></div>
      )}
    </>
  );
};

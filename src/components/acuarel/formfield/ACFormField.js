import React from 'react';

export const ACFormField = ({
  showFormHeader = false,
  formHeader = 'Form Header',
  showFormLabel = false,
  formForLabel = '',
  formTitleLabel = 'Label',
  formInputType = 'text',
  formInputClasses = '',
  formInputId = '',
  formInputValue = '',
  formInputName = '',
  onSubmit,
  onChange,
  formInputPlaceholder = 'Placeholder',
  ariaDescribedby = '',
  formInputAutocomplete = 'none',
}) => {
  return (
    <div className="form-group">
      {showFormHeader && <h4 className="AC-form__header">{formHeader}</h4>}
      {showFormLabel && (
        <label className="form-label" htmlFor={formForLabel}>
          {formTitleLabel}
        </label>
      )}
      <input
        type={formInputType}
        className={`form-control ${formInputClasses}`}
        id={formInputId}
        value={formInputValue}
        name={formInputName}
        onSubmit={onSubmit}
        onChange={onChange}
        placeholder={formInputPlaceholder}
        autoComplete={formInputAutocomplete}
        aria-describedby={ariaDescribedby}
      />
    </div>
  );
};

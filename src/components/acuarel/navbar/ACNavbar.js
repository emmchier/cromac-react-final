import React from 'react';
import { NavLink } from 'react-router-dom';
import { useMediaQueries } from '../../../hooks/useMediaQueries';

export const ACNavbar = ({
  brandImg = null,
  brandText = 'Acuarel',
  brandPosition = 'center',
  navList = null,
  navPosition = 'end',
  navFixed = false,
  toogleAlwaysVisible = false,
  tooglePosition = 'end',
  toggleButton = [],
  classes = '',
}) => {
  const { isDesk } = useMediaQueries();

  return (
    <nav
      className={
        navFixed
          ? `AC-navbar ${classes} AC-navbar--fixed navbar navbar-expand-lg`
          : `AC-navbar ${classes} navbar navbar-expand-lg`
      }
    >
      <div className="container">
        <div className="AC-navbar__brandContent AC-alignX">
          <NavLink
            className={`
                AC-navbar__navbarBrand 
                AC-navbar__navbarBrand--${brandPosition} 
                navbar-brand`}
            to="/"
          >
            {brandImg === null ? (
              brandText
            ) : (
              <img className="AC-navbar__navbarBrandImg" src={brandImg} alt={'brand logo'} />
            )}
          </NavLink>
        </div>
        <div
          className={
            isDesk
              ? `AC-navbar__nav AC-navbar__nav--${navPosition}`
              : `AC-navbar__nav AC-navbar__nav--hidden AC-navbar__nav--${navPosition}`
          }
        >
          <ul className="navbar-nav AC-alignX">{navList && navList}</ul>
        </div>
        <span
          className={
            toogleAlwaysVisible
              ? `AC-toggleButton AC-toggleButton--${tooglePosition}`
              : `AC-toggleButton AC-toggleButton--hidden`
          }
        >
          {toggleButton && toggleButton}
        </span>
      </div>
    </nav>
  );
};

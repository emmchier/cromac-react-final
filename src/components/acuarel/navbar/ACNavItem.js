import React from 'react';
import { NavLink } from 'react-router-dom';

export const ACNavItem = ({ item = {}, classes = '', to = '#!', onClick }) => {
  return (
    <li className={`AC-navItem ${classes} nav-item`}>
      <NavLink to={to} onClick={onClick} className="nav-link">
        {item}
      </NavLink>
    </li>
  );
};

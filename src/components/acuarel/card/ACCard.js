import React from 'react';

export const ACCard = ({
  cardImage = `./assets/image-default.svg`,
  cardImageAlt = 'item image',
  cardTitle = 'Card Title',
  cardDesc = 'Card Description',
  isImg = true,
  objectRender = null,
}) => {
  return (
    <div className="AC-card card">
      {objectRender !== null ? (
        objectRender
      ) : (
        <div>
          {isImg && <img src={cardImage} className="card-img-top" alt={cardImageAlt} />}
          <div className="card-body">
            <h5 className="card-title">{cardTitle}</h5>
            <p className="card-text">{cardDesc}</p>
          </div>
        </div>
      )}
    </div>
  );
};

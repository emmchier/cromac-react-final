import React from 'react';

export const ACTag = ({ tagName = 'Tag Name' }) => {
  return <li className="AC-tag">#{tagName}</li>;
};

import React from 'react';

import { SparkMultiple } from '../ui/spark/SparkMultiple';

export const Burguer = ({ onClick }) => {
  const burguerLines = [
    { id: 0, sparkLines: 'sparkLines1' },
    { id: 1, sparkLines: 'sparkLines2' },
    { id: 2, sparkLines: 'sparkLines3' },
  ];

  return (
    <div className="AC-burguerIcon" onClick={onClick}>
      {burguerLines.map((line) => (
        <SparkMultiple key={line.id} classes={line.sparkLines} />
      ))}
    </div>
  );
};

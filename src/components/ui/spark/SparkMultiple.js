import React from 'react';
import { SparkComponent } from './SparkComponent';

export const SparkMultiple = ({ number = 3, classes = '' }) => {
  return (
    <ul className={`${classes} sparkList`}>
      {[...new Array(number)].map((n, key) => (
        <li key={key}>
          <SparkComponent />
        </li>
      ))}
    </ul>
  );
};

import React from 'react';

export const SparkComponent = ({ classes = '' }) => {
  return <div className={`spark ${classes}`}></div>;
};

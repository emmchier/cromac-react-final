import React from 'react';

import { SparkComponent } from './SparkComponent';

export const Sparks = ({ number = 0, classes = '' }) => {
  return (
    <ul className={`${classes} sparks`}>
      {[...new Array(number)].map((n, key) => (
        <li key={key}>
          <SparkComponent />
        </li>
      ))}
    </ul>
  );
};

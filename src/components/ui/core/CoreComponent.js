import React from 'react';

import { SparkComponent } from '../spark/SparkComponent';

export const CoreComponent = () => {
  return (
    <div className="core">
      <SparkComponent classes={'mainCore'} />
      <SparkComponent classes={'ringCore'} />
    </div>
  );
};

import React from 'react';

export const Line = ({ classes = '' }) => {
  return <div className={`customLine ${classes}`}></div>;
};

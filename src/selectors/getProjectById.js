import { getProjects } from '../data/getData';

export const getProjectById = (id) => {
  return getProjects.find((project) => project.itemId === id);
};

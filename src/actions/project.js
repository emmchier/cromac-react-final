import { getProjects } from '../data/getData';
import { types } from '../types/types';

export const getAllProjects = () => ({
  type: types.projectsGetAll,
  payload: {
    projectList: getProjects,
  },
});

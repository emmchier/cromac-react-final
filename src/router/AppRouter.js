import React, { useState } from 'react';

import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

// custom components
import { NavList } from '../components/lists/NavList';
import { NetworkList } from '../components/lists/NetworkList';
import { Burguer } from '../components/burguer/Burguer';
import { Footer } from '../components/footer/Footer';

// acuarel components
import { ACNavbar } from '../components/acuarel/navbar/ACNavbar';
import { ACOverlay } from '../components/acuarel/overlay/ACOverlay';
import { ACSidenav } from '../components/acuarel/sidenav/ACSidenav';
import { ACButton } from '../components/acuarel/button/ACButton';

// pages
import { ContactScreen } from '../pages/contact/ContactScreen';
import { ErrorScreen } from '../pages/error/ErrorScreen';
import { HomeScreen } from '../pages/home/HomeScreen';
import { PolicyScreen } from '../pages/legal/PolicyScreen';
import { TermsScreen } from '../pages/legal/TermsScreen';
import { ProjectDetailScreen } from '../pages/projects/ProjectDetailScreen';
import { ProjectsScreen } from '../pages/projects/ProjectsScreen';
import { ServicesScreen } from '../pages/services/ServicesScreen';
import { TeamScreen } from '../pages/team/TeamScreen';

export const AppRouter = () => {
  const [showSidenav, setShowSidenav] = useState(false);

  const showSidebar = () => setShowSidenav(!showSidenav);

  return (
    <Router>
      <header>
        <ACNavbar
          brandImg={`./assets/brand/typography.svg`}
          navFixed={true}
          toogleAlwaysVisible={true}
          tooglePosition={'start'}
          toggleButton={<Burguer onClick={showSidebar} />}
        />
      </header>
      <ACSidenav
        isOpenSidenav={showSidenav}
        sidenavContent={
          <>
            <NavList
              onClick={() => {
                setShowSidenav(!showSidenav);
              }}
            />
            <ACButton
              isLink={true}
              to={'/contacto'}
              onClick={() => {
                setShowSidenav(!showSidenav);
              }}
              btnTitle={'Charlemos'}
              classes={'btnContact AC-button AC-button__outlined'}
              btnIcon={'arrow_backward'}
              isIconLeftVisible={true}
            />
            <NetworkList />
          </>
        }
        onClickClose={() => {
          setShowSidenav(!showSidenav);
        }}
        closeToggle={<i className="material-icons">close</i>}
      />
      <ACOverlay
        isOpenOverlay={showSidenav}
        onClickOverlay={() => {
          setShowSidenav(!showSidenav);
        }}
      />
      <main>
        <Switch>
          <Route exact path="/" component={HomeScreen} />
          <Route exact path="/proyectos" component={ProjectsScreen} />
          <Route exact path="/proyecto/:projectId" component={ProjectDetailScreen} />
          <Route exact path="/que-hacemos" component={ServicesScreen} />
          <Route exact path="/equipo" component={TeamScreen} />
          <Route exact path="/politicas-de-privacidad" component={PolicyScreen} />
          <Route exact path="/terminos-y-condiciones" component={TermsScreen} />
          <Route exact path="/contacto" component={ContactScreen} />
          <Route exact path="/upsss" component={ErrorScreen} />

          <Redirect to="/" />
        </Switch>
      </main>
      <Footer />
    </Router>
  );
};
